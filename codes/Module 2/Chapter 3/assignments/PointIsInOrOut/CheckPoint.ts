function check() {
    var p1: HTMLInputElement = <HTMLInputElement>document.getElementById("px");
    var p2: HTMLInputElement = <HTMLInputElement>document.getElementById("py");
    var a1: HTMLInputElement = <HTMLInputElement>document.getElementById("x1");
    var a2: HTMLInputElement = <HTMLInputElement>document.getElementById("y1");
    var b1: HTMLInputElement = <HTMLInputElement>document.getElementById("x2");
    var b2: HTMLInputElement = <HTMLInputElement>document.getElementById("y2");
    var c1: HTMLInputElement = <HTMLInputElement>document.getElementById("x3");
    var c2: HTMLInputElement = <HTMLInputElement>document.getElementById("y3");
    var ptx = parseFloat(p1.value);
    var pty = parseFloat(p2.value);
    var x1 = parseFloat(a1.value);
    var y1 = parseFloat(a2.value);
    var x2 = parseFloat(b1.value);
    var y2 = parseFloat(b2.value);
    var x3 = parseFloat(c1.value);
    var y3 = parseFloat(c2.value);
    var A_abc: number;
    var A_pab: number;
    var A_pbc: number;
    var A_pac: number;
    var sum: number;
    A_abc = (x1 * Math.abs(y2 - y3) + x2 * Math.abs(y3 - y1) + x3 * Math.abs(y1 - y2)) / 2;
    A_pab = (ptx * Math.abs(y1 - y2) + x1 * Math.abs(y2 - pty) + x2 * Math.abs(pty - y1)) / 2;
    A_pbc = (ptx * Math.abs(y2 - y3) + x2 * Math.abs(y3 - pty) + x3 * Math.abs(pty - y2)) / 2;
    A_pac = (ptx * Math.abs(y1 - y3) + x1 * Math.abs(y3 - pty) + x3 * Math.abs(pty - y1)) / 2;
    sum = A_pab + A_pac + A_pbc;
    if(Math.abs(A_abc - sum)<0.000001){
        document.getElementById("display").innerHTML = "Point Lies Inside Of The Triangle!";
    }
    else{
        document.getElementById("display").innerHTML = "No, The Point Does'nt Lie Inside Of The Triangle!";

    }

}



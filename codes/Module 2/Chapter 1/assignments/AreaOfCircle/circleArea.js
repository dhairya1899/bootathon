function area() {
    var r = document.getElementById("r");
    var ans = document.getElementById("ans");
    var area = Math.PI * Math.pow(parseFloat(r.value), 2);
    ans.value = area.toString();
    ans.innerHTML = "<br> The answer is " + area.toString();
}
//# sourceMappingURL=circleArea.js.map
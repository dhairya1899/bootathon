//this function takes x in form of degrees
function calcDeg() {
    var x: HTMLInputElement = <HTMLInputElement>document.getElementById("x");
    var ans: HTMLInputElement = <HTMLInputElement>document.getElementById("ans");
    var x1:number = parseFloat(x.value);
    var ansFinal:number = x1 + Math.cos(x1*Math.PI/180);
    ans.value = ansFinal.toString(); 
}

//radian = degrees *PI / 180

//this functon takes x in form of radian
function calcRad() {
    var x: HTMLInputElement = <HTMLInputElement>document.getElementById("x");
    var ans: HTMLInputElement = <HTMLInputElement>document.getElementById("ans");
    var x1:number = parseFloat(x.value);
    var ansFinal:number = x1 + Math.cos(x1*180/Math.PI);
    ans.value = ansFinal.toString(); 
}


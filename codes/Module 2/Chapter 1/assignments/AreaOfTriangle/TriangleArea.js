//fnc to cal. area of triangle
function area() {
    var x1 = document.getElementById("x1");
    var y1 = document.getElementById("y1");
    var x2 = document.getElementById("x2");
    var y2 = document.getElementById("y2");
    var x3 = document.getElementById("x3");
    var y3 = document.getElementById("y3");
    var ans = document.getElementById("ans");
    var x11 = parseFloat(x1.value);
    var y12 = parseFloat(y1.value);
    var x21 = parseFloat(x2.value);
    var y22 = parseFloat(y2.value);
    var x31 = parseFloat(x3.value);
    var y32 = parseFloat(y3.value);
    var a = Math.sqrt(Math.pow(Math.abs(x21 - x11), 2) + Math.pow(Math.abs(y22 - y12), 2));
    var b = Math.sqrt(Math.pow(Math.abs(x11 - x31), 2) + Math.pow(Math.abs(y12 - y32), 2));
    var c = Math.sqrt(Math.pow(Math.abs(x31 - x21), 2) + Math.pow(Math.abs(y32 - y22), 2));
    var s;
    var area;
    s = (a + b + c) / 2;
    area = Math.sqrt(Math.abs(s * (s - a) * (s - b) * (s - c)));
    console.log("S:" + s);
    console.log("a:" + a);
    console.log("b:" + b);
    console.log("c:" + c);
    console.log("x1:" + x11);
    console.log("y1:" + y12);
    console.log("x2:" + x21);
    console.log("y2:" + y22);
    console.log("x3:" + x31);
    console.log("y3:" + y32);
    console.log("ans:" + area);
    ans.value = area.toString();
}
//# sourceMappingURL=TriangleArea.js.map
function parseSeries() {
    /**
     *  This function parser the series of number.
     *  Ans differntiate it into negative values , postive values and zeros.
     *  Gives the negative and postive numbers and total numbers for each.
     *  Gives the total  number of zeros in series.
     */
    let t1 = document.getElementById("t1");
    let pos = document.getElementById("ans1");
    let neg = document.getElementById("ans2");
    let zero = document.getElementById("ans3");
    var str = t1.value;
    var postivevalues = ""; // To store postive values
    var negativevalues = ""; // To store negative values
    var postiveValues = 0; // To count the postive number.
    var negativeValues = 0; // To count the negative number.
    var zeroValues = 0; // To count the positive number.
    var count;
    // Try block will handle error, if user enter non numneric value.
    // in that condition it will throw the error and catch block will alert the message.
    try {
        var splitted = str.split(",");
        console.log(splitted);
        if (splitted.length == 0) {
            alert("No inputs are given.");
        }
        else if (splitted.length == 1) {
            count = 1;
        }
        else {
            count = splitted.length;
        }
        for (var i = 0; i < count; i++) {
            if (isNaN(eval(splitted[i]))) {
                console.log(eval(splitted[i]));
                throw new Error("Enter Valid Inputs");
            }
            else {
                if (splitted[i][0] == '+') {
                    postivevalues += splitted[i] + ",";
                    console.log("Plus");
                    postiveValues += 1; // Increment total  number of postive number
                }
                else if (splitted[i][0] == '-') {
                    negativevalues += splitted[i] + ",";
                    console.log("Minus");
                    negativeValues += 1; // Increment total  number of negative number
                }
                else if (splitted[i][0] == '0') {
                    console.log("xero");
                    zeroValues += 1; // Increment total  number of zeros
                }
                else {
                    postivevalues += splitted[i] + ",";
                    console.log("Plus");
                    postiveValues += 1; // Increment total  number of positive number
                }
            }
        }
        if (postiveValues > 0) {
            postivevalues = postivevalues.substr(0, postivevalues.length - 1);
        }
        if (negativeValues > 0) {
            negativevalues = negativevalues.substr(0, negativevalues.length - 1);
        }
        console.log("yes");
        pos.innerHTML = "Positive Values are :" + postivevalues.toString() + "." + "<br> Total Postive numbers are :" + postiveValues.toString();
        neg.innerHTML = "Negative Values are :" + negativevalues.toString() + "." + "<br> Total negative numbers are :" + negativeValues.toString();
        zero.innerHTML = "<br> Total zeros are :" + zeroValues.toString();
    }
    catch (Error) {
        alert("Enter valid input");
    }
}
//# sourceMappingURL=findno.js.map